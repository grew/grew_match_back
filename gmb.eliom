open Eliom_lib
open Eliom_content
open Html.D

open Grewlib

open Gmb_global
open Gmb_utils
open Gmb_types
open Gmb_main

module Gmb_app =
  Eliom_registration.App (
  struct
    let application_name = "gmb"
    let global_data_path = None
  end)

(* ========================================================================================================== *)
(* External services: supposed to be called from Jenkins *)
(* ========================================================================================================== *)

(* -------------------------------------------------------------------------------- *)
(* ping service *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["ping"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.unit
      ))
    (fun () () ->
       Log.info "<ping>";
       Lwt.return ("", "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* refresh_all service *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["refresh_all"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.unit
      ))
    (fun () corpus_id ->
       Log.info "<refresh_all>";
       let json = wrap 
        (fun () ->
          Table.refresh_all ();
          `Null
        ) () in
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* refresh_all service *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["reload"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.unit
      ))
    (fun () corpus_id ->
       Log.info "<reload>";
       let json = wrap 
        (fun () ->
          load_data ();
          `Null
        ) () in
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* refresh_corpus service *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
~path:(Eliom_service.Path ["refresh_corpus"])
~meth:(Eliom_service.Post (
    Eliom_parameter.unit,
    Eliom_parameter.string "corpus_id"
  ))
(fun () corpus_id ->
   Log.info "<refresh_corpus> corpus_id=%s" corpus_id;
   let json = wrap 
    (fun () ->
      Table.refresh_one corpus_id;
      `Null
    ) () in
   Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
)

(* ========================================================================================================== *)
(* Internal services: supposed to be called from grew_match *)
(* ========================================================================================================== *)

let register_service (service_name, service_fct, full_log) =
  Eliom_registration.String.create
    ~path:(Eliom_service.Path [service_name])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.string "param"
      ))
    (fun () param_string ->
      if full_log
      then Log.info "<%s> param=%s" service_name param_string
      else Log.info "<%s>" service_name;
       let json = wrap 
        (fun () -> 
          let param = Yojson.Basic.from_string param_string in
          service_fct param
        ) () in
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = List.map register_service [
  ("search", search, true);
  ("count", count, true);
  ("more_results", more_results, true);
  ("export", export, true);
  ("conll_export", conll_export, true);
  ("conll", conll, true);
  ("parallel", parallel, true);
  ("save", save, true);
  ("get_corpora_desc", get_corpora_desc, false);
  ("get_build_file", get_build_file, true);
  ("dowload_tgz", dowload_tgz, true);
]
